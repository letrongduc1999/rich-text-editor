import React, { useEffect, useMemo, useState, useCallback } from "react";
import {
  createEditor,
  Transforms,
  Editor,
  Text,
  Range,
  Element as SlateElement,
} from "slate";
import isUrl from "is-url";
import {
  Slate,
  Editable,
  withReact,
  useSlate,
  useSlateStatic,
  ReactEditor,
} from "slate-react";
import { cx, css } from "@emotion/css";

const LIST_TYPES = ["numbered-list", "bulleted-list"];

function App() {
  const editor = useMemo(() => withReact(createEditor()), []);
  const [value, setValue] = useState(initialValue);
  const [search, setSearch] = useState("");
  const [selectedText, setSelectedText] = useState("");
  const [color, setColor] = useState(false);
  const renderElement = useCallback((props) => <Element {...props} />, []);
  const renderLeaf = useCallback((props) => <Leaf {...props} />, []);

  const changeColor = (editor) => {
    console.log(editor);
    setColor(true);
  };

  const decorate = useCallback(
    ([node, path]) => {
      const ranges = [];

      if (search && Text.isText(node)) {
        const { text } = node;
        const parts = text.split(search);
        let offset = 0;

        parts.forEach((part, i) => {
          if (i !== 0) {
            ranges.push({
              anchor: { path, offset: offset - search.length },
              focus: { path, offset },
              highlight: true,
            });
          }

          offset = offset + part.length + search.length;
        });
      }
      if (color && Text.isText(node)) {
        const { text } = node;
        const parts = text.split(selectedText);
        let offset = 0;

        parts.forEach((part, i) => {
          if (i !== 0) {
            ranges.push({
              anchor: { path, offset: offset - search.length },
              focus: { path, offset },
              color: "green",
            });
          }

          offset = offset + part.length + search.length;
        });
      }
      setColor(false);
      return ranges;
    },
    [search, color]
  );

  return (
    <div
      style={{
        maxWidth: "800px",
        marginTop: "100px",
        marginLeft: "auto",
        marginRight: "auto",
        border: "1px solid black",
        padding: "18px 17px",
      }}
    >
      <Slate
        editor={editor}
        value={value}
        onChange={(value) => setValue(value)}
        // plugins={plugins}
      >
        <div
          className={cx(
            css`
              position: relative;
              display: flex;
              justify-content: space-between;
              border-bottom: 2px solid #eee;
              margin-bottom: 20px;
            `
          )}
        >
          <div
            className={cx(
              css`
                display: flex;
              `
            )}
          >
            <MarkButton format="bold" icon="format_bold" />
            <MarkButton format="italic" icon="format_italic" />
            <MarkButton format="underline" icon="format_underlined" />
            <MarkButton format="code" icon="code" />
            <BlockButton format="heading-one" icon="looks_one" />
            <BlockButton format="heading-two" icon="looks_two" />
            <BlockButton format="numbered-list" icon="format_list_numbered" />
            <BlockButton format="bulleted-list" icon="format_list_bulleted" />
            <LinkButton />
            <RemoveLinkButton />
          </div>
          <div
            className={css`
              position: relative;
            `}
          >
            <span
              className={cx(
                "material-icons",
                css`
                  position: absolute;
                  top: 0.1em;
                  left: 0.1em;
                  color: #ccc;
                `
              )}
            >
              search
            </span>
            <input
              type="search"
              placeholder="Search the text..."
              onChange={(e) => setSearch(e.target.value)}
              className={css`
                padding-left: 2em;
                width: 100%;
                height: 32px;
                cursor: pointer;
                margin-bottom: 10px;
              `}
            />
          </div>
        </div>
        <Editable
          renderElement={renderElement}
          renderLeaf={renderLeaf}
          decorate={decorate}
          spellCheck
          autoFocus
        />
      </Slate>
    </div>
  );
}

const toggleFormat = (editor, format) => {
  const isActive = isFormatActive(editor, format);
  Transforms.setNodes(
    editor,
    { [format]: isActive ? null : true },
    { match: Text.isText, split: true }
  );
};

const isFormatActive = (editor, format) => {
  const [match] = Editor.nodes(editor, {
    match: (n) => n[format] === true,
    mode: "all",
  });
  return !!match;
};

const toggleBlock = (editor, format) => {
  const isActive = isBlockActive(editor, format);
  const isList = LIST_TYPES.includes(format);

  Transforms.unwrapNodes(editor, {
    match: (n) =>
      LIST_TYPES.includes(
        !Editor.isEditor(n) && SlateElement.isElement(n) && n.type
      ),
    split: true,
  });
  const newProperties = {
    type: isActive ? "paragraph" : isList ? "list-item" : format,
  };
  Transforms.setNodes(editor, newProperties);

  if (!isActive && isList) {
    const block = { type: format, children: [] };
    Transforms.wrapNodes(editor, block);
  }
};

const toggleMark = (editor, format) => {
  const isActive = isMarkActive(editor, format);

  if (isActive) {
    Editor.removeMark(editor, format);
  } else {
    Editor.addMark(editor, format, true);
  }
};

const isBlockActive = (editor, format) => {
  const [match] = Editor.nodes(editor, {
    match: (n) =>
      !Editor.isEditor(n) && SlateElement.isElement(n) && n.type === format,
  });

  return !!match;
};

const isMarkActive = (editor, format) => {
  const marks = Editor.marks(editor);
  return marks ? marks[format] === true : false;
};

const withLinks = (editor) => {
  const { insertData, insertText, isInline } = editor;

  editor.isInline = (element) => {
    return element.type === "link" ? true : isInline(element);
  };

  editor.insertText = (text) => {
    if (text && isUrl(text)) {
      wrapLink(editor, text);
    } else {
      insertText(text);
    }
  };

  editor.insertData = (data) => {
    const text = data.getData("text/plain");

    if (text && isUrl(text)) {
      wrapLink(editor, text);
    } else {
      insertData(data);
    }
  };

  return editor;
};

const insertLink = (editor, url) => {
  if (editor.selection) {
    wrapLink(editor, url);
  }
};

const isLinkActive = (editor) => {
  const [link] = Editor.nodes(editor, {
    match: (n) =>
      !Editor.isEditor(n) && SlateElement.isElement(n) && n.type === "link",
  });
  return !!link;
};

const unwrapLink = (editor) => {
  Transforms.unwrapNodes(editor, {
    match: (n) =>
      !Editor.isEditor(n) && SlateElement.isElement(n) && n.type === "link",
  });
};

const wrapLink = (editor, url) => {
  if (isLinkActive(editor)) {
    unwrapLink(editor);
  }

  const { selection } = editor;
  const isCollapsed = selection && Range.isCollapsed(selection);
  const link = {
    type: "link",
    url,
    children: isCollapsed ? [{ text: url }] : [],
  };

  if (isCollapsed) {
    Transforms.insertNodes(editor, link);
  } else {
    Transforms.wrapNodes(editor, link, { split: true });
    Transforms.collapse(editor, { edge: "end" });
  }
};

const VideoElement = ({ attributes, children, element }) => {
  const editor = useSlateStatic();
  const { url } = element;
  return (
    <div {...attributes}>
      <div contentEditable={false}>
        <div
          style={{
            padding: "75% 0 0 0",
            position: "relative",
          }}
        >
          <iframe
            src={`${url}`}
            frameBorder="0"
            style={{
              position: "absolute",
              top: "0",
              left: "0",
              width: "100%",
              height: "100%",
            }}
          />
        </div>
        <YoutubeUrl
          url={url}
          onChange={(val) => {
            const path = ReactEditor.findPath(editor, element);
            const newProperties = {
              url: val,
            };
            Transforms.setNodes(editor, newProperties, { at: path });
          }}
        />
      </div>
      {children}
    </div>
  );
};

const YoutubeUrl = ({ url, onChange }) => {
  const [value, setValue] = useState(url);
  return (
    <input
      value={value}
      onClick={(e) => e.stopPropagation()}
      style={{
        marginTop: "5px",
        padding: "5px",
        width: "300px",
        boxSizing: "border-box",
      }}
      onChange={(e) => {
        const newUrl = e.target.value;
        setValue(newUrl);
        onChange(newUrl);
      }}
    />
  );
};

const LinkButton = () => {
  const editor = useSlate();
  return (
    <span
      active={isLinkActive(editor)}
      onMouseDown={(event) => {
        event.preventDefault();
        const url = window.prompt("Enter the URL of the link:");
        if (!url) return;
        insertLink(editor, url);
      }}
      className={css`
        margin-right: 10px;
      `}
    >
      <span
        className={cx(
          "material-icons",
          css`
            cursor: pointer;
            color: ${isLinkActive(editor) ? "black" : "#ccc"};
          `
        )}
      >
        link
      </span>
    </span>
  );
};

const RemoveLinkButton = () => {
  const editor = useSlate();

  return (
    <span
      active={isLinkActive(editor)}
      onMouseDown={(event) => {
        if (isLinkActive(editor)) {
          unwrapLink(editor);
        }
      }}
      className={css`
        margin-right: 10px;
      `}
    >
      <span
        className={cx(
          "material-icons",
          css`
            cursor: pointer;
            color: ${isLinkActive(editor) ? "black" : "#ccc"};
          `
        )}
      >
        link_off
      </span>
    </span>
  );
};

const Element = ({ attributes, children, element }) => {
  switch (element.type) {
    case "bulleted-list":
      return <ul {...attributes}>{children}</ul>;
    case "heading-one":
      return <h1 {...attributes}>{children}</h1>;
    case "heading-two":
      return <h2 {...attributes}>{children}</h2>;
    case "list-item":
      return <li {...attributes}>{children}</li>;
    case "numbered-list":
      return <ol {...attributes}>{children}</ol>;
    case "video":
      return <VideoElement {...attributes} element={element} {...children}/>;
    case "link":
      return (
        <a {...attributes} href={element.url}>
          {children}
        </a>
      );
    default:
      return <p {...attributes}>{children}</p>;
  }
};

const Leaf = ({ attributes, children, leaf }) => {
  if (leaf.bold) {
    children = <strong>{children}</strong>;
  }

  if (leaf.code) {
    children = <code>{children}</code>;
  }

  if (leaf.italic) {
    children = <em>{children}</em>;
  }

  if (leaf.underline) {
    children = <u>{children}</u>;
  }

  return (
    <span
      className={css`
        background-color: ${leaf.highlight && "#ffeeba"};
        color: ${leaf.color && "black"};
      `}
      {...attributes}
    >
      {children}
    </span>
  );
};

const BlockButton = ({ format, icon }) => {
  const editor = useSlate();
  return (
    <div
      active={isBlockActive(editor, format)}
      onMouseDown={(event) => {
        event.preventDefault();
        toggleBlock(editor, format);
      }}
      className={css`
        margin-right: 10px;
      `}
    >
      <span
        className={cx(
          "material-icons",
          css`
            cursor: pointer;
            color: ${isMarkActive(editor, format) ? "black" : "#ccc"};
          `
        )}
      >
        {icon}
      </span>
    </div>
  );
};

const MarkButton = ({ format, icon }) => {
  const editor = useSlate();
  return (
    <div
      active={isMarkActive(editor, format)}
      onMouseDown={(event) => {
        event.preventDefault();
        toggleMark(editor, format);
      }}
      className={css`
        margin-right: 10px;
      `}
    >
      <span
        className={cx(
          "material-icons",
          css`
            cursor: pointer;
            color: ${isMarkActive(editor, format) ? "black" : "#aaa"};
          `
        )}
      >
        {icon}
      </span>
    </div>
  );
};

const initialValue = [
  {
    type: "paragraph",
    children: [
      { text: "Ryu Hayabusa", bold: true },
      { text: " is a fictional character who serves as the " },
      { text: "main protagonist", bold: true },
      { text: " of Tecmo's " },
      { text: "Ninja Gaiden ", italic: true },
      { text: "action-adventure video game series. He is a " },
      { text: "dragon-human hybrid", code: true },
      {
        text: " who wields an ancestral weapon called the Dragon Sword, and is the leader of the Hayabusa ninja clan. One of Tecmo's most enduring characters, Ryu has appeared on official series merchandise as well as the feature film DOA: Dead or Alive, and has made many crossover appearances in other games. He has received public and critical reception as one of the most iconic ninjas in video games.",
      },
    ],
  },
  {
    type: "paragraph",
    children: [
      {
        text: "As the son of renowned ninja ",
      },
      { text: "Joe Hayabusa", bold: true },
      {
        text: ", Ryu Hayabusa (whose first and last names, respectively, translate literally to 'dragon' and 'peregrine falcon') is the wielder of the legendary Dragon Sword.",
      },
    ],
  },
  {
    type: "paragraph",
    children: [
      {
        text: "Since his 1988 debut, Ryu Hayabusa has been both critically and publicly received as a popular character in general video gaming",
      },
    ],
  },
  {
    type: 'video',
    url: 'https://www.youtube.com/embed/kjcotB7E6oE',
    children: [{ text: '' }],
  },
  {
    type: "paragraph",
    children: [{ text: "Try to type something please" }],
  },
  
];

export default App;
